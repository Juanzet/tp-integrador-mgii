﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bat : MonoBehaviour
{
    [SerializeField] GameObject prefabPlayer;
    [SerializeField] public Transform player;
    [SerializeField] private float distance;
    [SerializeField] Text defeatTXT;

    public Vector3 inicialPoint;
    private Animator anim;
    private SpriteRenderer spriteRenderer;
    void Start()
    {
        anim = GetComponent<Animator>();
        inicialPoint = transform.position;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
 
    void Update()
    {
        distance = Vector2.Distance(transform.position, player.position);
        anim.SetFloat("Distancia", distance);
    }

    public void FlipEnemy(Vector3 pPlayer)
    {
        if(transform.position.x < pPlayer.x)
        {
            spriteRenderer.flipX = true;
        } else
        {
            spriteRenderer.flipX = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            defeatTXT.text = "You Die, Press M to Reset";
            Destroy(prefabPlayer);
        }
        if(collision.tag=="Rubber")
        {
            Destroy(this.gameObject);
        }
    }

}
