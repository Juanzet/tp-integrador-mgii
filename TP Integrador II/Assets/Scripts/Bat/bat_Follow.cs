﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bat_Follow : StateMachineBehaviour
{
    [SerializeField] private float movSpeed; // velocidad a la que se mueve el murcielago
    [SerializeField] private float baseTime; // tiempo base para seguir al jugador
    private float timeToFollow=4; // tiempo total
    private Transform player; // posicion deel jugador
    private Bat bat; // referencia al script del murcielago

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timeToFollow = baseTime;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        bat = animator.gameObject.GetComponent<Bat>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.transform.position = Vector2.MoveTowards(animator.transform.position, player.position, movSpeed * Time.deltaTime);
        bat.FlipEnemy(player.position);
        timeToFollow -= Time.deltaTime;
        if(timeToFollow <=0)
        {
            animator.SetTrigger("Volver");
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateinfo, int layerindex)
    {

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
