﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _Speed;
    [SerializeField] private float _JumpForce;
    private Rigidbody2D _Rb;
    private bool canJump = false;
    [SerializeField] private SpriteRenderer rubber;
    [SerializeField] private SpriteRenderer pencil;
    [SerializeField] private Sprite rubberSprite;
    [SerializeField] private Sprite pencilSprite;
    [SerializeField] private Animator anim;

    void Start()
    {
           _Rb = GetComponent<Rigidbody2D>();
        rubber = GetComponent<SpriteRenderer>();
        pencil = GetComponent<SpriteRenderer>();
          anim = GetComponent<Animator>();
    }
    void Update()
    {
        PlayerMovement();
        TransformToRubber();
    }

    void PlayerMovement()
    {
        
        float mov = Input.GetAxis("Horizontal");
        transform.position += new Vector3(mov, 0, 0) * Time.deltaTime * _Speed;

        if (!Mathf.Approximately(0, mov))
            transform.rotation = mov > 0 ? Quaternion.Euler(0, -180, 0) : Quaternion.identity;

        if (Input.GetKeyDown(KeyCode.W) && canJump == true)
        {
            _Rb.AddForce(new Vector2(0, _JumpForce), ForceMode2D.Impulse);
        }
    }

    void TransformToRubber()
    {
        if(Input.GetKey(KeyCode.R))
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = rubberSprite;
            this.gameObject.tag = "Rubber";
            //pencil.gameObject.SetActive(false);
            //rubber.gameObject.SetActive(true);
            //rubber.transform.position = new Vector2(pencil.transform.position.x, pencil.transform.position.y);
        } 

        if(Input.GetKeyUp(KeyCode.R))
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = pencilSprite;
            this.gameObject.tag = "Player";
            //pencil.transform.position = new Vector2(rubber.transform.position.x, rubber.transform.position.y);
            //pencil.gameObject.SetActive(true);
            //rubber.gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            canJump = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            canJump = false;
        }
    }
}
