﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ReseetGameScene01GP : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKey(KeyCode.M))
        {
            SceneManager.LoadScene("Scene01GP");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            SceneManager.LoadScene("Scene01GP");
        }
    }
}
