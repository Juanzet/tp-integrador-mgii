﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsManager : MonoBehaviour
{

    public void Level01()
    {
        SceneManager.LoadScene("Scene03GP");
    }

    public void Level02()
    {
        SceneManager.LoadScene("Scene02GP");
    }

    public void level03()
    {
        SceneManager.LoadScene("Scene01GP");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

}
